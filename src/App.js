import logo from './logo.svg';
import './App.css';
import Timer from './components/Timer';
import { useState } from 'react';

function App() {
  const [toggle, setToggle] = useState(false);
  return (
    <div>
      { toggle ? <p>Alternate component</p> : <Timer/> }
      <button onClick={() => setToggle(!toggle)}>Toggle</button>
    </div>
  );
}

export default App;
